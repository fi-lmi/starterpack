FROM jupyter/minimal-notebook:python-3.10 as base
USER root

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Europe/Prague

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y libxml2 g++ vim coreutils nano git libxrender1 tmux zip \
    wget fakeroot tzdata locales curl procps ca-certificates tini \
    build-essential libssl-dev zlib1g-dev

RUN ln -fs /usr/share/zoneinfo/Europe/Prague /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen

# Install OpenSSH from source
ENV OPENSSH_VERSION=9.8p1
RUN wget https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-${OPENSSH_VERSION}.tar.gz && \
    tar zxvf openssh-${OPENSSH_VERSION}.tar.gz && \
    cd openssh-${OPENSSH_VERSION} && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    rm -rf openssh-${OPENSSH_VERSION}* && \
    ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key

COPY start.sh .
RUN chmod a+x start.sh && \
    mkdir .ssh && \
    chown -R jovyan /home/jovyan/.ssh /etc/ssh /run && \
    touch /home/jovyan/.ssh/authorized_keys && \
    chown jovyan /home/jovyan/. /home/jovyan/.ssh/ /home/jovyan/.ssh/authorized_keys && \
    chmod u+rwX,go-rwX,-t /home/jovyan/.ssh/ /home/jovyan/.ssh/authorized_keys && \
    chmod go-w /home/jovyan/

USER jovyan

ENV SHELL /bin/bash
ENV PATH /usr/local/bin:/opt/conda/bin:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin

EXPOSE 2222

WORKDIR /

ENTRYPOINT ["/usr/bin/tini", "--"]

CMD ["/home/jovyan/start.sh"]
