#!/bin/bash

export TERM=xterm-256color

# Initilize conda in .bashrc
cat <<EOF >>~/.bashrc
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="\$('/opt/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ \$? -eq 0 ]; then
    eval "\$__conda_setup"
else
    if [ -f "/opt/conda/etc/profile.d/conda.sh" ]; then
        . "/opt/conda/etc/profile.d/conda.sh"
    else
        export PATH="/opt/conda/bin:\$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
EOF

echo "$SSH_KEY" >>~/.ssh/authorized_keys

if ! grep -q KUBERNETES ~/.bashrc 2>/dev/null; then
    set | grep 'PROXY\|KUBERNETES' | sed -e 's/^/export /' >>~/.bashrc
fi

# Start the ssh service
/usr/local/sbin/sshd -e -p 2222 -h /etc/ssh/ssh_host_ed25519_key -o PasswordAuthentication=no -o AuthorizedKeysFile=/home/jovyan/.ssh/authorized_keys

# Start the JupyterLab
# Option 1) Run JupyterLab, connect to it solely via SSH tunneling
jupyter-lab --no-browser --ip "0.0.0.0" --port 8888 --NotebookApp.password=''

# Option 2) - Advanced: Set up a password for JupyterLab so that you can access it from the browser
# To do this, you need an already existing jupyterlab instance.
# 1. Run `jupyter lab password`, provide a password
# 2. Copy /home/<user>/.jupyter/jupyter_server_config.json into /data/<username>/.jupyter/jupyter_server_config.json
# 3. Un-comment the following line and comment line 39
# JUPYTER_CONFIG_DIR=/data/<username>/.jupyter jupyter-lab --no-browser --ip "0.0.0.0" --port 8888